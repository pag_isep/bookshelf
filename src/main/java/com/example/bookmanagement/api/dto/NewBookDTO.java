package com.example.bookmanagement.api.dto;

import lombok.Data;

@Data
public class NewBookDTO {

	private String isbn;

	private String title;

	private String authorRef;
}
