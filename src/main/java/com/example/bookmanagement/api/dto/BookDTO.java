package com.example.bookmanagement.api.dto;

import lombok.Data;

@Data
public class BookDTO {

	private String isbn;

	private String title;

	private String authorRef;

	private String authorName;
}
