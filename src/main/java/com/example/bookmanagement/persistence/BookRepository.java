package com.example.bookmanagement.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.example.bookmanagement.model.Book;

/**
 * based on https://www.baeldung.com/spring-boot-start
 * 
 * @author SOU03408
 *
 */
public interface BookRepository extends CrudRepository<Book, Long> {

	List<Book> findByTitleLike(String title);

	Optional<Book> findByIsbn(String isbn);

	List<Book> findByAuthor(String author);
}
