package com.example.bookmanagement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * 
 * @author SOU03408
 *
 */
@Entity
public class Author {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(nullable = false, unique = true)
	private final String authorRef;

	@Column(nullable = false)
	private String name;

	private String bio;

	// for ORM only
	protected Author() {
		authorRef = "";
	}

	public Author(String ref, String name) {
		this.authorRef = ref;
		this.name = name;
	}

	public Author(String ref, String name, String bio) {
		this(ref, name);
		setBio(bio);
	}

	public String getAuthorRef() {
		return authorRef;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

}