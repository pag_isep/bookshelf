package com.example.bookmanagement.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * based on https://www.baeldung.com/spring-boot-start
 * 
 * @author SOU03408
 *
 */
@Entity
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(nullable = false, unique = true)
	private final String isbn;

	@Column(nullable = false, unique = false)
	private String title;

	@ManyToOne(optional = false)
	private Author author;

	private String summary;

	// for ORM only
	protected Book() {
		isbn = "";
	}

	public Book(String isbn, String title, Author author) {
		this.isbn = isbn;
		this.title = title;
		this.author = author;
	}

	public Book(String isbn, String title, Author author, String summary) {
		this(isbn, title, author);
		setSummary(summary);
	}

	public String getIsbn() {
		return isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}
}